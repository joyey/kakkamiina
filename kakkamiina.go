package main

// Ugly pre-calculated minesweeper game

import (
	"fmt"
	"github.com/nsf/termbox-go"
	"math/rand"
	"time"
)

const (
	XScale, YScale  = 5, 2
	MineValue       = 666 // TODO: Refactor out ugly value hacks
	BorderValue     = 777
	MineProbability = 10
)

type Cell struct {
	value                   int
	mined, visible, flagged bool
}

// fuuuuu
type Coords struct {
	X, Y int
}

type Score struct {
	Mines, Found, Flags int
}

type Field struct {
	width, height int
	cells         []Cell
}

func (f Field) CellAt(x, y int) *Cell {
	if x < 0 || x > f.width-1 || y < 0 || y > f.height-1 {
		return &Cell{value: BorderValue, visible: true}
	}
	return &(f.cells[y*f.width+x])
}

func (f *Field) Reveal(x, y int) {
	c := f.CellAt(x, y)
	c.visible = true
	neighbors := neighborCoords(x, y)
	for _, n := range neighbors {
		if c2 := f.CellAt(n.X, n.Y); c.value == 0 && !c2.visible {
			f.Reveal(n.X, n.Y)
		}
	}
}

func (f *Field) RevealAll() {
	for i := range f.cells {
		f.cells[i].visible = true
	}
}

func neighborCoords(x, y int) []Coords {
	return []Coords{
		Coords{x - 1, y - 1},
		Coords{x, y - 1},
		Coords{x + 1, y - 1},
		Coords{x - 1, y},
		Coords{x + 1, y},
		Coords{x - 1, y + 1},
		Coords{x, y + 1},
		Coords{x + 1, y + 1},
	}
}

func (f Field) AllVisibleAround(x, y int) bool {
	for _, coord := range neighborCoords(x, y) {
		if n := f.CellAt(coord.X, coord.Y); !n.visible && !n.mined {
			return false
		}
	}
	return true
}

func (f *Field) RevealFound() {
	for y := 0; y < f.height; y++ {
		for x := 0; x < f.width; x++ {
			if c := f.CellAt(x, y); c.mined && f.AllVisibleAround(x, y) {
				c.visible = true
			}
		}
	}
}

func (f *Field) Flag(x, y int) {
	if c := f.CellAt(x, y); c.flagged {
		c.flagged = false
	} else {
		c.flagged = true
	}
}

func (f *Field) precalculate() {
	for y := 0; y <= f.height-1; y++ {
		for x := 0; x <= f.width-1; x++ {
			currCell := f.CellAt(x, y)
			for _, coords := range neighborCoords(x, y) {
				if f.CellAt(coords.X, coords.Y).mined {
					currCell.value++
				}
			}
		}
	}
}

func (f Field) countScore() Score {
	s := Score{}
	for _, c := range f.cells {
		if c.mined {
			s.Mines++
		}
		if c.flagged {
			s.Flags++
		}
		if c.mined && c.visible {
			s.Found++
		}
	}
	return s
}

func (f Field) Winning() bool {
	for _, c := range f.cells {
		if c.mined && !(c.visible || c.flagged) {
			return false
		}
		if c.flagged && !c.mined {
			return false
		}
	}
	return true
}

func NewField(width, height int) Field {
	numCells := width * height
	cells := make([]Cell, numCells)
	// Mine stuff first
	for i := range cells {
		if rand.Intn(MineProbability) == 0 {
			cells[i].mined = true
			cells[i].value = MineValue
		}
	}
	f := Field{width: width, height: height, cells: cells}
	f.precalculate()
	return f
}

func drawGrid(width, height, selectedX, selectedY int) {
	for y := 0; y <= height*YScale; y++ {
		for x := 0; x <= width*XScale; x++ {
			var r rune
			if x == 0 {
				if y == 0 {
					r = '╔'
				} else if y == height*YScale {
					r = '╚'
				} else if y%YScale == 0 {
					r = '╠'
				} else {
					r = '║'
				}
			} else if x == width*XScale {
				if y == 0 {
					r = '╗'
				} else if y == height*YScale {
					r = '╝'
				} else if y%YScale == 0 {
					r = '╣'
				} else {
					r = '║'
				}
			} else if x%XScale == 0 {
				if y == 0 {
					r = '╦'
				} else if y == height*YScale {
					r = '╩'
				} else if y%YScale == 0 {
					r = '╬'
				} else {
					r = '║'
				}
			} else {
				if y%YScale == 0 {
					r = '═'
				} else {
					r = '░'
				}
			}
			fg := termbox.ColorWhite
			if (x >= selectedX*XScale && x <= (selectedX+1)*XScale) && (y >= selectedY*YScale && y <= (selectedY+1)*YScale) {
				if x%XScale == 0 || y%YScale == 0 {
					fg = termbox.ColorCyan
				}
			}
			termbox.SetCell(x, y, r, fg, termbox.ColorBlack)
		}
	}
}

func renderField(f Field, selectedX, selectedY int) {
	drawGrid(f.width, f.height, selectedX, selectedY)
	for i, c := range f.cells {
		x := (i%f.width)*XScale + (XScale / 2)
		y := (i/f.height)*YScale + (YScale / 2)
		if c.visible {
			termbox.SetCell(x-1, y, ' ', termbox.ColorWhite, termbox.ColorBlack)
			termbox.SetCell(x, y, ' ', termbox.ColorWhite, termbox.ColorBlack)
			termbox.SetCell(x+1, y, ' ', termbox.ColorWhite, termbox.ColorBlack)
			termbox.SetCell(x+2, y, ' ', termbox.ColorWhite, termbox.ColorBlack)
			if c.value > 0 && c.value < 10 {
				termbox.SetCell(x, y, rune(fmt.Sprintf("%d", c.value)[0]), termbox.ColorRed, termbox.ColorBlack)
			}
			if c.mined {
				termbox.SetCell(x, y, '💩', termbox.ColorWhite, termbox.ColorBlack)
			}
		}
		if c.flagged && !(c.mined && c.visible) {
			termbox.SetCell(x, y, '🚩', termbox.ColorRed, termbox.ColorBlack)
		}
	}
}

func renderScore(x, y, mines, foundmines, flags int) {
	found := foundmines + flags
	s := fmt.Sprintf("Mines: %d Found: %d", mines, found)
	if flags > 0 {
		s = s + "?"
	}
	putString(x, y, s, termbox.ColorYellow, termbox.ColorBlack)
}

func putString(x, y int, s string, fg, bg termbox.Attribute) {
	for i, r := range s {
		termbox.SetCell(x+i, y, r, fg, bg)
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	f := NewField(10, 10)
	termbox.Init()
	defer termbox.Close()
	var quit, winning, lost bool
	var selectedX, selectedY int
	var msg string // :<
	for !quit {
		renderField(f, selectedX, selectedY)
		s := f.countScore()
		renderScore(0, f.height*YScale+2, s.Mines, s.Found, s.Flags)

		winning = f.Winning()
		if lost {
			msg = "YOU LOOSED :-----("
			f.RevealAll()
			renderField(f, selectedX, selectedY)
		} else if winning {
			msg = "!!! YOU WIN !!!"
		}

		if winning || lost {
			x := f.width*XScale/2 - len(msg)/2
			y := f.width * YScale / 2
			putString(x, y, msg, termbox.ColorYellow, termbox.ColorBlack)
			quit = true
		}

		termbox.Flush()

		switch termbox.PollEvent().Key {
		case termbox.KeyArrowDown:
			if selectedY < f.height-1 {
				selectedY++
			} else {
				selectedY = 0
			}
		case termbox.KeyArrowUp:
			if selectedY > 0 {
				selectedY--
			} else {
				selectedY = f.height - 1
			}
		case termbox.KeyArrowLeft:
			if selectedX > 0 {
				selectedX--
			} else {
				selectedX = f.width - 1
			}
		case termbox.KeyArrowRight:
			if selectedX < f.width-1 {
				selectedX++
			} else {
				selectedX = 0
			}
		case termbox.KeyEnter:
			f.Flag(selectedX, selectedY)
		case termbox.KeySpace:
			f.Reveal(selectedX, selectedY)
			f.RevealFound()
			if f.CellAt(selectedX, selectedY).mined {
				lost = true
			}
		case termbox.KeyEsc:
			quit = true
		}
	}
}
