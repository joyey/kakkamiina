# Kakkamiina

Simple terminal based mine sweeper

![screenshot](screenshot.png "woah")

## Requirements

An UTF-8-friendly terminal and a font that supports emojis. This game was written with double-character-wide emojis in mind (think macOS) but should work elsewhere as well.

## Usage

This is a very basic 10x10 grid minesweeper. Move the cyan cursor with cursor keys, reveal playfield cells with Space and flag cells with Enter.

If you have either successfully flagged or revealed all mines in the playfield you will win. If you hit a mine you lose. Simple and efficient.

